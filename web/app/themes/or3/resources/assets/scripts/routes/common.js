import gsap from 'gsap';
import 'slick-slider';
import Cookies from 'js-cookie';
import LazyLoad from 'vanilla-lazyload';

export default {
    init() {

        const lazyLoadOptions = {/* your options here */};
        new LazyLoad(lazyLoadOptions);

        $('.cookies-ok').on('click', function() {
            Cookies.set('or3_cookies', 'ok', { expires: 365 });
            $('.cookie-consent').fadeOut();
        });

        $('.page-hero').css('height', $(window).height());

        $('.form-control').on('keyup keypress blur change', function() {
            if ($(this).val()) {
                $(this).closest('.form-group').addClass('filled');
            } else {
                $(this).closest('.form-group').removeClass('filled');
            }
        });

        $('.form-control').on('focus', function() {
            $(this).closest('.form-group').addClass('focused');
        });

        $('.form-control').on('blur', function() {
            $(this).closest('.form-group').removeClass('focused');
        });

        // gsap.to('.brand', {duration: 1, x: 100});


        const menuOpenClose = gsap.timeline({ paused: true });
        menuOpenClose.to('.main-nav-container', { duration: 0.25, opacity: 0 });
        menuOpenClose.add(function() {

            if (menuOpenClose.reversed()) {
                $('.toggle-menu-btn').removeClass('close-menu');
                $('.toggle-menu-btn').addClass('open-menu');
                $('body').removeClass('extended-menu-open');
                $('.sliding-menu').removeClass('d-none');


            } else {
                $('.sliding-menu').addClass('d-none');
                $('.toggle-menu-btn').removeClass('open-menu');
                $('.toggle-menu-btn').addClass('close-menu');
                $('body').addClass('extended-menu-open');

            }

        });
        menuOpenClose.to('.header-wrap', { duration: 0.3, background: '#fff' });
        menuOpenClose.to('.header-wrap', { duration: 0.3, height: '100%' }, 0);
        menuOpenClose.add(function() {

            if (menuOpenClose.reversed()) {
                $('.header-extended').hide();
            } else {
                $('.header-extended').show();
            }

        });
        menuOpenClose.to('.header-wrap .ext-col:nth-child(1) .inner', { duration: 0.25, opacity: 1, y: 0 });
        menuOpenClose.to('.header-wrap .ext-col:nth-child(2) .inner', { duration: 0.25, opacity: 1, y: 0 });
        menuOpenClose.to('.header-wrap .ext-col:nth-child(3) .inner', { duration: 0.25, opacity: 1, y: 0 });

        $('body').on('click', '.open-menu', function() {
            menuOpenClose.play();
        });

        $('body').on('click', '.close-menu', function() {
            menuOpenClose.reverse();
        });


        if($('.scroll-down span').length) {
        const scrdown = gsap.timeline({ repeat: -1, repeatDelay: 0.7 });
        scrdown.to('.scroll-down span', { duration: 1, x: 20 });
        scrdown.to('.scroll-down span', { duration: 1, x: 0 });
        scrdown.play();
        }

        let last_known_scroll_position = 0;
        let ticking = false;

        let current_sliding_item;

        function doSomething(scroll_pos) {

            if (scroll_pos > 200) {
                $('.header-wrap').addClass('has-background');
            } else {
                $('.header-wrap').removeClass('has-background');
            }

            if (scroll_pos > ($(window).outerHeight() / 2)) {
                $('.sticky-button').addClass('visible');
            } else {
                $('.sticky-button').removeClass('visible');
            }



            $('.value-tree-item .dot').each(function(){
                var dot = $(this);
                if($(dot).offset().top < (scroll_pos + ($(window).outerHeight() / 2))) {
                    $(dot).addClass('filled');
                } else {
                    $(dot).removeClass('filled');
                }

            });


            if($('.sliding-menu-anchor')) {

                $('.sliding-menu-anchor').each(function(){

                let elem = $(this);

                    if(scroll_pos > ($(elem).offset().top -200)) {
                        $(elem).addClass('scrolled');
                    } else {
                        $(elem).removeClass('scrolled');
                    }

                });

                let cur_id = $('.sliding-menu-anchor.scrolled').last().attr('id');
                if(typeof cur_id != 'undefined' && cur_id != current_sliding_item) {

                    current_sliding_item = cur_id;
                    $('[data-sliding-id]').removeClass('active');
                    $('[data-sliding-id="'+cur_id+'"]').addClass('active');

                    let scrollAmount = $('[data-sliding-id="'+cur_id+'"]').position().left;

                    console.log('Offset before: ' + scrollAmount);
                  
                    $('.sliding-menu').stop().animate({scrollLeft: scrollAmount}, 500, 'swing', function() { 
                        console.log('Offset After: ' + $('[data-sliding-id="'+cur_id+'"]').position().left);
                        console.log('Scrolled: ' + $('.sliding-menu').scrollLeft());
                        console.log('+++++++++++');
                     });

                }



            }


        }


        window.addEventListener('scroll', function() {
            last_known_scroll_position = window.scrollY;


            if (!ticking) {
                window.requestAnimationFrame(function() {
                    doSomething(last_known_scroll_position);
                    ticking = false;
                });

                ticking = true;
            }

        });

        $('.scroll-down').on('click', function(e) {
            e.preventDefault();
            $('html, body').stop();
            $('html, body').animate({ scrollTop: $(window).outerHeight() }, 600);
            $(window).bind('mousewheel', function() {
                $('html, body').stop();
            });
        });




        $('.circle-image-carousel').each(function() {
            $(this).slick({
                infinite: false,
                appendArrows: $(this).closest('.circle-image-container').next(),
            });
        });


        $('.item-tabs-btn').on('click', function() {

            let cont = $(this).closest('.item-body-tabs');
            let tab = $(this).data('open-slide');

            $(cont).find('.item-tabs-slide').removeClass('active');
            $(cont).find('.item-tabs-slide[data-slide=' + tab + ']').addClass('active');

            $(cont).find('.item-tabs-btn').removeClass('active');
            $(this).addClass('active');

        });

        $('.item-second-level-tabs-btn').on('click', function() {

            let cont = $(this).closest('.item-tabs-slide');
            let tab = $(this).data('open-slide');

            $(cont).find('.second-level-tabs-slide').removeClass('active');
            $(cont).find('.second-level-tabs-slide[data-2nd-slide=' + tab + ']').addClass('active');

            $(cont).find('.item-second-level-tabs-btn').removeClass('active');
            $(this).addClass('active');

        });


        $('.carousel-circle-slides').each(function() {

            let carouselDots = $(this).closest('.container').find('.carousel-circle-dot-marker');
            let container = $(this).closest('.container').find('.carousel-circle-container');
            let dot = $(carouselDots).find('.marker-dot');
            let navfor = $(this).data('navfor');

            $(this).slick({
                infinite: false,
                arrows: false,
                dots: true,
                fade: true,
                speed: 500,
                asNavFor: navfor,
                customPaging: function(slick, index) {
                    let num = (index + 1);
                    num = num.toString().padStart(2, '0');
                    return '<button>' + num + '</button>';
                },
            });

            $(this).on('beforeChange', function(event, slick, currentSlide, nextSlide) {

                let newpos = $(container).find('[data-index=' + nextSlide + ']').data('position');

                gsap.to(dot, { duration: 0.1, width: 10, height: 10 });
                gsap.to(carouselDots, { duration: 0.5, rotate: newpos });
                gsap.to(dot, { duration: 0.3, width: 35, height: 35, delay: 0.5 });

            });
        });

        $('.carousel-circle-btn').on('click', function() {
            let goto = $(this).data('goto-slide');

            $(this).closest('.circular-carousel').find('.carousel-circle-slides').slick('slickGoTo', goto);
            console.log(goto);
        });

        $('.in-circle-slider').each(function() {
            let navfor = $(this).data('navfor');

            $(this).slick({
                infinite: false,
                arrows: false,
                dots: false,
                fade: true,
                speed: 800,
                asNavFor: navfor,
                height: '100%',
            });
        });



        $('.testimonials-slider').each(function() {


            $(this).slick({
                infinite: false,
                arrows: true,
                dots: true,
                fade: false,
                speed: 500,
                customPaging: function(slick, index) {
                    var author = slick.$slides.eq(index).find('[data-author]').data('author');
                    var position = slick.$slides.eq(index).find('[data-position]').data('position');
                    var img = slick.$slides.eq(index).find('[data-image]').data('image');
                    return '<div class="custom-dot"><div class="img" style="background-image: url(' + img + ');"></div><button><span class="author">' + author + '</span><span class="position">' + position + '</span></button></div>';
                },
                appendArrows: $(this).next('.testimonials-arrows'),
            });
        });


        $('.key-fatcs-carousel').each(function() {
            let navfor = $(this).data('navfor');

            $(this).slick({
                infinite: false,
                arrows: false,
                dots: true,
                fade: true,
                speed: 500,
                asNavFor: navfor,
                customPaging: function(slick, index) {
                    let num = (index + 1);
                    num = num.toString().padStart(2, '0');
                    return '<button>' + num + '</button>';
                },
            });
        });


        $('.key-facts-image-carousel').each(function() {
            let navfor = $(this).data('navfor');

            $(this).slick({
                infinite: false,
                arrows: false,
                dots: false,
                fade: false,
                speed: 800,
                asNavFor: navfor,
                height: '100%',
            });
        });


        $('.horizontal-content-carousel').each(function() {
            let navfor = $(this).data('navfor');

            $(this).slick({
                infinite: false,
                arrows: false,
                dots: true,
                fade: false,
                speed: 500,
                asNavFor: navfor,
                customPaging: function(slick, index) {
                    let num = (index + 1);
                    num = num.toString().padStart(2, '0');
                    return '<button>' + num + '</button>';
                },
            });
        });


        $('.horizontal-images-carousel').each(function() {
            let navfor = $(this).data('navfor');

            let autoplay = false;
            if ($(this).data('autoplay')) {
                autoplay = true;
            }

            $(this).slick({
                infinite: true,
                arrows: false,
                dots: false,
                fade: true,
                speed: 800,
                asNavFor: navfor,
                height: '100%',
                autoplay: autoplay,
            });
        });

        $('.main-title-items').each(function() {
            $(this).slick({
                infinite: true,
                arrows: false,
                dots: false,
                fade: true,
                speed: 500,
                autoplay: true,
                autoplaySpeed: 4000,
            });
        });

        $('.certs-carousel').each(function() {
            $(this).slick();
        });

        $('.video-overlay').on('click', function() {
            $(this).parent().find('video').get(0).play();
            $(this).fadeOut();
            $(this).parent().find('video').show();
        });

        $('.get-quote-btn, .cf-close').on('click', function(e) {
            e.preventDefault();
            $('.quote-form-holder').toggleClass('open');
            $('body').toggleClass('cf-open');
        });


    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired



    },
};