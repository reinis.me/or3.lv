import 'slick-slider';

export default {
  init() {
    // JavaScript to be fired on the home page

    const settings = {
        fade: true,
        infinite: false,
        responsive: [
          {
            breakpoint: 799,
            settings: 'unslick',
          },
        ],
    };

    const sl =  $('.products-archive-categories').slick(settings);

    $(window).on('resize', function() {
      if( $(window).width() > 799 &&  !sl.hasClass('slick-initialized')) {
            $('.products-archive-categories').slick(settings);
       }
   });

    $('[data-cat-slide]').on('click', function() {
        let index = $(this).data('cat-slide');
        $('.products-archive-categories').slick('slickGoTo', index, false);
    });

    $('.products-archive-categories').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $('[data-cat-slide]').removeClass('active');
        $('[data-cat-slide='+nextSlide+']').addClass('active');
    });

  },
  finalize() {
    // JavaScript to be fired on the home page, after the init JS
  },
};
