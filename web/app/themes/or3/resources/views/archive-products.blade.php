@extends('layouts.app')

@section('content')

<?php 

$product_categories = get_terms( array(
    'taxonomy' => 'product-categories',
    'hide_empty' => false,
) );

?>

<div class="container container-outer margin-top">

    <div class="row justify-content-center">
        <div class="col-12 col-xl-10">

            <div class="products-archive-menu-container">
                <h1><?php _e('Products', 'or3'); ?></h1>
                <ul class="products-archive-menu">
                    <?php
                foreach ($product_categories as $key => $cat) {
                    ?>
                    <li><button type="button"
                            class="products-archive-menu-btn <?php if($key == 0) { echo "active"; } ?>"
                            data-cat-slide="<?php echo $key; ?>"><?php echo $cat->name; ?></button></li>
                    <?php
                }

            ?>
                </ul>
            </div>

            <div class="products-archive-categories">
                <?php
                foreach ($product_categories as $cat) {
                    ?>

                <div class="content-list-item" data-slug="<?php echo $cat->slug; ?>">
                    <div class="sliding-menu-anchor" id="<?php echo $cat->slug; ?>"></div>
                    <div class="inner">
                        <div class="item-images">
                            @include('partials.image-circle', ['images' => array(get_field('image',
                            'term_'.$cat->term_id))])
                        </div>

                        <div class="item-body">
                            <h2><?php echo $cat->name; ?></h2>

                            <div class="item-body-text">
                                <?php the_field('content', 'term_'.$cat->term_id); ?>
                            </div>

                            <div>
                                <a href="<?php echo get_term_link($cat->term_id); ?>" title="<?php echo $cat->name; ?>"
                                    class="btn btn-inline btn-secondary"><?php _e('Learn more', 'or3'); ?></a>

                            </div>
                        </div>
                    </div>

                </div>

                <?php
                }

            ?>
            </div>
        </div>
    </div>
</div>

@endsection