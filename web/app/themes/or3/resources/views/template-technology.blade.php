{{--
  Template Name: Technology
--}}

@extends('layouts.app')

@section('content')

<section class="common-header-header">
    <div class="container container-outer">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">
                <div class="row">
                    <div class="col-12 col-md-6 header-content-middle">
                        <div class="watermark"></div>
                        <h1 class="common-header-title"><?php the_title(); ?></h1>
                    </div>
                    <div class="col-12 col-md-6 img-col">
                        <?php
if(get_post_thumbnail_id()) {
    $background = 'style="background-image: url(' . wp_get_attachment_image_src(get_post_thumbnail_id(), 'large')[0] .')"';
} else {
    $background = '';
}

?>
                        <div class="common-header-image no-margin" <?php echo $background; ?>></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="scroll-down"><span>Scroll down</span></div>

</section>

@while(have_posts()) @php the_post() @endphp
{{-- @include('partials.page-header') --}}
@include('partials.content-page')
@endwhile
@endsection