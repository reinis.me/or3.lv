{{--
  Template Name: News
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')
  @endwhile
  <a href="/contacts/" class="sticky-button">Contact us</a>
@endsection
