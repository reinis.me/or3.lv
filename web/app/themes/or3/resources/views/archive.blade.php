@extends('layouts.app')

@section('content')

<section class="common-header-header">
    <div class="container container-outer">
        <div class="row">
            <div class="col-12 col-md-6 header-content-middle">
                <div class="watermark"></div>
                <h1 class="common-header-title"><?php the_field('services_header_title', 'option'); ?></h1>
            </div>
            <div class="col-12 col-md-6 img-col">
                <?php
                    if(get_field('services_header_image', 'option')) {
                        $background = 'style="background-image: url(' . wp_get_attachment_image_src(get_field('services_header_image', 'option'), 'large')[0] .')"';
                    } else {
                        $background = '';
                    }

                    ?>
                <div class="common-header-image no-margin" <?php echo $background; ?>></div>
            </div>
        </div>
    </div>
    <div class="scroll-down"><span>Scroll down</span></div>

</section>



<section class="product-items">
    <div class="container">
        @while (have_posts()) @php the_post() @endphp

        <div class="content-list-item in-list">
            <div class="inner">
                <div class="item-images">
                    @include('partials.image-circle', ['images' => get_field('images')])
                </div>

                <div class="item-body">
                    <h2><?php the_title(); ?></h2>

                <div><?php the_field('description'); ?></div>

          
                </div>
            </div>

        </div>

        @endwhile
    </div>
</section>


@endsection