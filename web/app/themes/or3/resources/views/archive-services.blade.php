@extends('layouts.app')

@section('content')

<div class="quote-form-holder">

    <div class="container container-outer">
        <div class="row justify-content-md-center">
            <div class="col-12 col-lg-10">
                <a href="#" class="cf-close">Close</a>
                <?php echo do_shortcode('[contact-form-7 id="817" title="Services"]'); ?>
            </div>
        </div>
    </div>

</div>

<a href="#" class="sticky-button get-quote-btn">Get Quote</a>


<section class="common-header-header">
    <div class="container container-outer">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">
                <div class="row">
                    <div class="col-12 col-md-6 header-content-middle">
                        <div class="watermark"></div>
                        <h1 class="common-header-title"><?php the_field('services_header_title', 'option'); ?></h1>
                    </div>
                    <div class="col-12 col-md-6 img-col">
                        <?php
                    if(get_field('services_header_image', 'option')) {
                        $background = 'style="background-image: url(' . wp_get_attachment_image_src(get_field('services_header_image', 'option'), 'large')[0] .')"';
                    } else {
                        $background = '';
                    }

                    ?>
                        <div class="common-header-image no-margin" <?php echo $background; ?>></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="scroll-down"><span>Scroll down</span></div>

</section>



<section class="product-items">
    <div class="container container-outer">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">
                <?php 
                global $query_string;
                $cat_posts = new WP_Query($query_string."&orderby=menu_order&order=ASC");
                ?>

                @while ($cat_posts->have_posts()) @php 
                $cat_posts->the_post(); 
                global $post;
                 $slug = $post->post_name;
                @endphp

                <div class="content-list-item in-list">
                    <div id="<?php echo $slug; ?>" class="anchor sliding-menu-anchor"></div>
                    <div class="inner">
                        <div class="item-images">
                            @include('partials.image-circle', ['images' => get_field('images')])
                        </div>

                        <div class="item-body">
                            <h2><?php the_title(); ?></h2>

                            <div><?php the_field('description'); ?></div>


                        </div>
                    </div>

                </div>

                @endwhile
            </div>
        </div>
    </div>
</section>


@endsection