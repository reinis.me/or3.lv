@extends('layouts.app')

@section('content')
  {{-- @include('partials.page-header') --}}

  <?php

$post_id = false;

if( is_home() )
{
	$post_id = get_option( 'page_for_posts' );; // specif ID of home page
}

?>

  <section class="common-header-header">
    <div class="container container-outer">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">
                <div class="row">
                    <div class="col-12 col-md-6 header-content-middle">
                        <div class="watermark"></div>
                        <h1 class="common-header-title"><?php the_field('services_header_title', $post_id); ?></h1>
                    </div>
                    <div class="col-12 col-md-6 img-col">
                        <?php
                if(get_field('services_header_image', $post_id)) {
                    $background = 'style="background-image: url(' . wp_get_attachment_image_src(get_field('services_header_image', $post_id), 'large')[0] .')"';
                } else {
                    $background = '';
                }

                ?>
                        <div class="common-header-image no-margin" <?php echo $background; ?>></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="scroll-down"><span>Scroll down</span></div>

</section>


  <section class="news-items">
    <div class="container container-outer">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">
        @while (have_posts()) @php the_post() @endphp

        <div class="content-list-item in-list">
            <div class="inner">
                <div class="item-images">
                    @include('partials.image-circle', ['images' => array(get_post_thumbnail_id())])
                </div>

                <div class="item-body">
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                <div class="item-excerpt"><?php the_excerpt(); ?></div>
              
                <div>
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
                      class="btn btn-inline btn-secondary"><?php _e('Read more', 'or3'); ?></a>

              </div>
          
                </div>
            </div>

        </div>

        @endwhile
    </div>
    </div>
    </div>
</section>

@endsection


