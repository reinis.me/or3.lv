@extends('layouts.app')

@section('content')

<div class="quote-form-holder">

    <div class="container container-outer">
        <div class="row justify-content-md-center">
            <div class="col-12 col-lg-10">
                <a href="#" class="cf-close"></a>
                <?php echo do_shortcode('[contact-form-7 id="816" title="Products"]'); ?>
            </div>
        </div>
    </div>

</div>

<a href="#" class="sticky-button get-quote-btn">Get Quote</a>


<?php 

$term = get_queried_object();

?>

<section class="common-header-header">
    <div class="container container-outer">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="watermark"></div>
                        <h1 class="common-header-title"><?php echo $term->name; ?></h1>
                        <div class="common-header-content">
                            <?php the_field('content', 'term_'.$term->term_id); ?>
                        </div>

                    </div>
                    <div class="col-12 col-md-6 img-col">
                        <?php
if(get_field('image', 'term_'.$term->term_id)) {
    $background = 'style="background-image: url(' . wp_get_attachment_image_src(get_field('image', 'term_'.$term->term_id), 'large')[0] .')"';
} else {
    $background = '';
}

?>
                        <div class="common-header-image no-margin" <?php echo $background; ?>></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="scroll-down"><span>Scroll down</span></div>

</section>

<div class="sticky-categories">
    <div class="title">
        <?php _e('Products', 'or3'); ?>
    </div>
    <div class="links">
        <?php 
    $product_categories = get_terms( array(
        'taxonomy' => 'product-categories',
        'hide_empty' => false,
    ) );
    foreach ($product_categories as $cat) {
    ?>
        <a class="cat-link <?php if($cat->term_id == $term->term_id){ echo "active"; } ?>"
            href="<?php echo get_term_link($cat->term_id); ?>" title="<?php echo $cat->name; ?>">
            <?php echo file_get_contents(get_attached_file(get_field('menu_icon', 'term_'.$cat->term_id))); ?>
        </a>

        <?php
    }
    ?>
    </div>
    <a class="cat-link" href="<?php echo get_taxonomy_archive_link('product-categories'); ?>"
        title="<?php _e('Back to all categories', 'or3'); ?>">
        <svg xmlns="http://www.w3.org/2000/svg" width="23" height="16.727" viewBox="0 0 23 16.727">
            <path id="Path_50" data-name="Path 50"
                d="M21.2,11.136H9.182l1.673-1.255A1.045,1.045,0,1,0,9.6,8.209L5.418,11.345c-.018.013-.024.034-.041.049a1.024,1.024,0,0,0-.2.242,1,1,0,0,0-.072.1.982.982,0,0,0,0,.877,1.018,1.018,0,0,0,.072.1,1.023,1.023,0,0,0,.2.242c.017.014.023.036.041.049L9.6,16.149a1.045,1.045,0,0,0,1.255-1.673L9.182,13.227H21.2a4.7,4.7,0,1,1,0,9.409H19.636a1.045,1.045,0,1,0,0,2.091H21.2a6.8,6.8,0,1,0,0-13.591Z"
                transform="translate(-5 -8)" fill="#171717" />
        </svg>
    </a>
</div>


<section class="product-items">
    <div class="container container-outer">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">
                <?php 
                global $query_string;
                $cat_posts = new WP_Query($query_string."&orderby=menu_order&order=ASC");
                ?>


                @while ($cat_posts->have_posts()) @php
                $cat_posts->the_post();
                global $post;
                $slug = $post->post_name;
                @endphp


                <div class="content-list-item in-list" data-slug="<?php echo $cat->slug; ?>">
                    <div id="<?php echo $slug; ?>" class="anchor sliding-menu-anchor"></div>

                    <div class="inner">
                        <div class="item-images">
                            @include('partials.image-circle', ['images' => get_field('images')])
                        </div>

                        <div class="item-body">
                            <h2><?php the_title(); ?></h2>

                            <div class="item-body-inner item-body-tabs">
                                <div class="item-tabs-container">
                                    <?php $i = 1; ?>
                                    <?php if(get_field('description')) { ?>
                                    <div class="item-tabs-slide <?php if($i == 1) {echo "active";} ?>"
                                        data-slide="<?php echo $i; ?>"><?php the_field('description'); ?></div>
                                    <?php $i++; } ?>
                                    <?php if(get_field('parameters')) { ?>
                                    <div class="item-tabs-slide <?php if($i == 1) {echo "active";} ?>"
                                        data-slide="<?php echo $i; ?>"><?php the_field('parameters'); ?></div>
                                    <?php $i++;} ?>
                                    <?php if(get_field('related_products') || get_field('related_services') || get_field('industries')) { ?>
                                    <div class="item-tabs-slide <?php if($i == 1) {echo "active";} ?>"
                                        data-slide="<?php echo $i; ?>">
                                        <?php $s = 1; ?>

                                        <div class="item-second-level-tabs-nav">
                                            <?php $s = 1; ?>
                                            <?php if(get_field('related_products')) { ?>
                                            <button type="button"
                                                class="item-second-level-tabs-btn <?php if($s == 1) {echo "active";} ?>"
                                                data-open-slide="<?php echo $s; ?>"><?php _e('Products', 'or3'); ?></button>
                                            <?php $s++;} ?>
                                            <?php if(get_field('related_services')) { ?>
                                            <button type="button"
                                                class="item-second-level-tabs-btn <?php if($s == 1) {echo "active";} ?>"
                                                data-open-slide="<?php echo $s; ?>"><?php _e('Services', 'or3'); ?></button>
                                            <?php $s++;} ?>
                                            <?php if(get_field('industries') || get_field('related_services') || get_field('industries')) { ?>
                                            <button type="button"
                                                class="item-second-level-tabs-btn <?php if($s == 1) {echo "active";} ?>"
                                                data-open-slide="<?php echo $s; ?>"><?php _e('Industries', 'or3'); ?></button>
                                            <?php $s++;} ?>
                                        </div>

                                        <div class="second-level-tabs-container">

                                            <?php $s = 1; ?>
                                            <?php if(get_field('related_products')) { ?>
                                            <div class="second-level-tabs-slide <?php if($s == 1) {echo "active";} ?>"
                                                data-2nd-slide="<?php echo $s; ?>"><ul>
                                                    <?php
                                                $products = get_field('related_products');
                                                foreach ($products as $key => $product) {
                                                    ?>
                                                    <li><a
                                                            href="<?php echo $product["link"]; ?>"><?php echo $product["title"]; ?></a>
                                                    </li>

                                                    <?php
                                                }
                                                ?>
                                                </ul>
                                            </div>
                                            <?php $s++;} ?>
                                            <?php if(get_field('related_services')) { ?>
                                            <div class="second-level-tabs-slide <?php if($s == 1) {echo "active";} ?>"
                                                data-2nd-slide="<?php echo $s; ?>">
                                                <ul>

                                                <?php
                                            $services = get_field('related_services');
                                            foreach ($services as $key => $service) {
                                                ?>
                                                <li><a
                                                        href="<?php echo $service["link"]; ?>"><?php echo $service["related_service"]->post_title ; ?></a>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                                </ul>
                                            </div>
                                            <?php $s++;} ?>
                                            <?php if(get_field('industries')) { ?>
                                            <div class="second-level-tabs-slide <?php if($s == 1) {echo "active";} ?>"
                                                data-2nd-slide="<?php echo $s; ?>">
                                                <?php print_r(get_field('industries')); ?>
                                            </div>
                                            <?php $s++;} ?>

                                        </div>



                                    </div>
                                    <?php $i++;} ?>
                                </div>
                                <div class="item-tabs-nav">
                                    <?php $i = 1; ?>
                                    <?php if(get_field('description')) { ?>
                                    <button type="button" class="item-tabs-btn <?php if($i == 1) {echo "active";} ?>"
                                        data-open-slide="<?php echo $i; ?>"><?php _e('About', 'or3'); ?></button>
                                    <?php $i++;} ?>
                                    <?php if(get_field('parameters')) { ?>
                                    <button type="button" class="item-tabs-btn <?php if($i == 1) {echo "active";} ?>"
                                        data-open-slide="<?php echo $i; ?>"><?php _e('Parameters', 'or3'); ?></button>
                                    <?php $i++;} ?>
                                    <?php if(get_field('related_products') || get_field('related_services') || get_field('industries')) { ?>
                                    <button type="button" class="item-tabs-btn <?php if($i == 1) {echo "active";} ?>"
                                        data-open-slide="<?php echo $i; ?>"><?php _e('Related Products, Services & Industries', 'or3'); ?></button>
                                    <?php $i++;} ?>
                                </div>

                            </div>


                        </div>
                    </div>

                </div>

                @endwhile


            </div>
        </div>
    </div>
</section>

@endsection