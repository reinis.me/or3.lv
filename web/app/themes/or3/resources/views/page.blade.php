@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    {{-- @include('partials.page-header') --}}
    @include('partials.content-page')
  @endwhile
  @php
    if ( is_front_page() ) :  @endphp
  <a href="/contacts/" class="sticky-button sticky-contacts">Contact us</a>

 @php endif; @endphp
@endsection
