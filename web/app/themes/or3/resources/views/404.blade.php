@extends('layouts.app')

@section('content')
  <div class="container container-404">
    <h1>Page not found!</h1>
    <p>Sorry, but the page you are trying to view does not exist.</p>
  </div>
@endsection
