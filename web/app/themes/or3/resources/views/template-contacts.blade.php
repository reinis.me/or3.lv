{{--
  Template Name: Contacts
--}}

@extends('layouts.app')

@section('content')

<section class="contacts-upper">
    <div class="container container-outer">
        <div class="row justify-content-md-center">
            <div class="col-12 col-lg-4">
                <div class="tinytitle"><?php _e('OR3', 'or3'); ?></div>
                <h1><?php the_field('title'); ?></h1>
            </div>
            <div class="col-12 col-lg-6">
                <div class="row">
                    <?php if (have_rows('columns')) : ?>
                    <?php while (have_rows('columns')) : the_row();
                    ?>

                    <div class="col-12 col-md-4">
                        <div class="content">
                            <h5><?php the_sub_field('title'); ?></h5>
                            <div class="descr"><?php the_sub_field('content'); ?></div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contacts-form">
    <div class="container container-outer">
        <div class="row justify-content-md-center">
            <div class="col-12 col-lg-10">
<?php echo do_shortcode('[contact-form-7 id="815" title="Untitled"]'); ?>
            </div>
        </div>
    </div>
</section>
<section class="contacts-map">
    <div class="container container-outer">
        <div class="map-container" id="map">

        </div>
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_ShejKGfBzX0xhvhAFoTGLSg8knCPUd0&callback=initMap"
            type="text/javascript"></script>
        <script type="text/javascript">
            // Initialize and add the map
function initMap() {
  // The location of Uluru
  const uluru = { lat: 56.871808, lng: 24.068468 };
  
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 9,
    center: uluru,
    styles: [
                {
                    "featureType": "administrative",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 65
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": "50"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": "-100"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "30"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "all",
                    "stylers": [
                        {
                            "lightness": "40"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#C9C9C9"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "lightness": -25
                        },
                        {
                            "saturation": -100
                        }
                    ]
                }
            ]
});
  // The marker, positioned at Uluru


  <?php
  $i=1;

if( have_rows('map') ): while ( have_rows('map') ) : the_row(); 
    if( have_rows('pins') ): while ( have_rows('pins') ) : the_row(); 
    ?>
    const marker_<?php echo $i; ?> = new google.maps.Marker({
    position: { lat: <?php the_sub_field('lat'); ?>, lng: <?php the_sub_field('lon'); ?> },
    map: map,
    icon: "@asset('images/pin.svg')"
    });
    
  <?php
    $i++;
    endwhile; endif;
    endwhile; endif;
    ?>
}

        </script>
    </div>
</section>

<section class="contacts-lower"></section>
@endsection