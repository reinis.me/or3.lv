<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10">

            <div class="about-boxes-top">
                <h2><?php the_field('title'); ?></h2>
                <div class="content">
                    <?php the_field('content'); ?>
                </div>
            </div>

            <div class="about-boxes-items">

                <div class="row">


                    <?php if (have_rows('boxes')) : ?>
                        <?php while (have_rows('boxes')) : the_row();
                        ?>

                            <div class="col-12 col-md boxes-col">
                                <div class="icon" style="background-image: url(<?php the_sub_field('icon'); ?>);"></div>
                                <div class="content">
                                    <h5><?php the_sub_field('title'); ?></h5>
                                    <div class="descr"><?php the_sub_field('content'); ?></div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>
</div>