<?php
if (get_field('slides')) {

    $slides = get_field('slides');
    $count = count($slides);
    $step = 360 / $count;
}


$mode = get_field('mode');

?>
<div class="container circular-carousel">
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="carousel-circle-container">
                        <?php
                        for ($i = 0; $i < $count; $i++) {
                        ?>
                            <div class="carousel-circle-dot" style="transform: rotate(<?php echo ($i * $step); ?>deg)" data-position="<?php echo ($i * $step); ?>" data-index="<?php echo $i; ?>">
                                <button class="carousel-circle-btn" data-goto-slide="<?php echo $i; ?>"></button>
                            </div>
                        <?php
                        }
                        ?>
                        <div class="carousel-circle-dot-marker" data-step="<?php echo $step; ?>">
                            <div class="marker-dot"></div>
                        </div>
                        <div class="in-circle-slider" id="sldr_c_<?php echo $block['id']; ?>" data-navfor="#sldr_m_<?php echo $block['id']; ?>">
                            <?php if (have_rows('slides')) : ?>
                                <?php while (have_rows('slides')) : the_row();
                                ?>
                                    <div class="slide">
                                        <div class="inner">
                                            <?php if ($mode == 1) { ?>
                                                <div class="slide-img" style="background-image: url(<?php the_sub_field('slide_image'); ?>);"></div>
                                            <?php } else { ?>
                                                <h6><?php the_sub_field('slide_title'); ?></h6>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div>
                    <div class="carousel-circle-slides" id="sldr_m_<?php echo $block['id']; ?>" data-navfor="#sldr_c_<?php echo $block['id']; ?>">
                        <?php if (have_rows('slides')) : ?>
                            <?php while (have_rows('slides')) : the_row();
                            ?>
                                <div class="slide">
                                    <h5><?php the_sub_field('slide_title'); ?></h5>
                                    <div class="descr"><?php the_sub_field('slide_content'); ?></div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
