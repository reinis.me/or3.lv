<section class="quality-safety">
    <div class="inner lazy" data-bg="<?php the_field('image'); ?>">
    <div class="overlay"></div>
    <div class="container">
    <h2><?php the_field('title'); ?></h2>
        <div class="content"><?php the_field('content'); ?></div>
        <div class="text-center">
            <a href="<?php echo get_field('link')['url']; ?>" class="btn btn-primary"><?php echo get_field('link')['title']; ?></a>
        </div>
    </div>
    </div>
</section>