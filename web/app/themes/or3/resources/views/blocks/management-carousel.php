<div class="container management-top">
    <h2><?php the_field('title'); ?></h2>
</div>

<div class="container management-items">
<div class="row justify-content-center">
            <div class="col-12 col-xl-10">
    <div class="row justify-content-center">


    <?php if (have_rows('items')) : ?>
        <?php while (have_rows('items')) : the_row();
        ?>

            <div class="col-12 col-lg-6 management-item">
                <div class="image" <?php if(get_sub_field('image')) { ?> style="background-image: url(<?php the_sub_field('image'); ?>);" <?php } ?>></div>
                <div class="content">
                    <h5><?php echo nl2br(get_sub_field('name')); ?></h5>
                    <div class="position"><?php the_sub_field('position'); ?></div>
                </div>
            </div> 
        <?php endwhile; ?>
    <?php endif; ?>

    </div>
            </div>
</div>
</div>