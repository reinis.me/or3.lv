<section class="industries">
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10">
            <div class="row">
                <div class="col-12 col-md-6 order-md-2 image-col">
                <div class="decoration"></div>    

                    <div class="img-zoom-in">
                        <div class="industries-image lazy" data-bg="<?php echo wp_get_attachment_image_src(get_field('image'), 'large')[0]; ?>">
                    </div>
                        
                    </div>
                </div>
                <div class="col-12 col-md-6 order-md-1">
                    <div class="industries-inner">
                        <div class="mini-title"><?php the_field('mini_title'); ?></div>
                        <h2><?php the_field('title'); ?></h2>
                        <div class="content"><?php the_field('content'); ?></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>