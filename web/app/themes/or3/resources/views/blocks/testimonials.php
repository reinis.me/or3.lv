<div class="container testimonials-top">
    <h2><?php the_field('title'); ?></h2>
</div>

<div class="row justify-content-center">
    <div class="col-12 col-xl-10">
        <div class="testimonials-slider">


            <?php if (have_rows('slides')) : ?>
                <?php while (have_rows('slides')) : the_row();

                $img = '';
                if(get_sub_field('author_image')) {
                    $img = get_sub_field('author_image')["sizes"]["medium"];
                }
                ?>

                    <div class="slide" data-author="<?php the_sub_field('author'); ?>" data-position="<?php echo htmlspecialchars(nl2br(get_sub_field('position'))); ?>" data-image="<?php echo $img; ?>">
                        <div class="content">
                            <div class="inner">
                                <?php the_sub_field('content'); ?>
                                <div class="inner-meta">
                                    <?php if ($img) { ?>
                                        <div class="img"><img class="lazy" data-src="<?php echo $img; ?>" alt="<?php the_sub_field('author'); ?>" /></div>
                                    <?php } ?>
                                    <div class="author"><?php the_sub_field('author'); ?></div>
                                    <div class="position"><?php echo nl2br(get_sub_field('position')); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>

        </div>
        <div class="testimonials-arrows"></div>
    </div>
</div>