<div class="row justify-content-center">
    <div class="col-12 col-xl-10">
        <div class="video-holder">
            <div class="decoration-upper"></div>
            <div class="decoration-lower"></div>
            <div class="decoration-watermark"></div>
            <video width="320" height="240" controls style="display: none;" preload="none">
                <source src="<?php the_field('video'); ?>" type="video/mp4">
            </video>
            <div class="video-overlay lazy" data-bg="<?php the_field('image'); ?>">
                <h2><?php the_field('title'); ?></h2>
                <div class="play-button"></div>
            </div>
        </div>
    </div>
</div>