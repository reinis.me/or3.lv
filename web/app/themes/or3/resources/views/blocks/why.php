<section class="why-people-trust">
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10">
            <div class="row">
                <div class="col-12 col-md-6">
                    <h2><?php the_field('title'); ?></h2>
                </div>
                <div class="col-6">
                    <div class="decoration-blue"></div>
                </div>
            </div>
            <div class="row">
                <?php if (have_rows('items')) : ?>
                    <?php while (have_rows('items')) : the_row();
                    ?>

                        <div class="col-12 col-md-6 col-lg-4"> 
                            <div class="trust-item">
                                <h4><?php the_sub_field('title'); ?></h4>
                                <div class="trust-content"><?php the_sub_field('content'); ?></div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>
                <div class="col-8">
                    <div class="decoration-gray"></div>
                </div>
            </div>
        </div>
    </div>
</section>