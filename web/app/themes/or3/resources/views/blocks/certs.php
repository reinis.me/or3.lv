<div class="container certs-items">
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10">
            <div class="row justify-content-center">

                <div class="certs-carousel">

                    <?php if (have_rows('items')) : ?>
                        <?php while (have_rows('items')) : the_row();
                        ?>
                            <div class="cert-slide">
                                <a href="<?php echo get_sub_field('certificate_image')['url']; ?>" target="_blank"><img src="<?php echo get_sub_field('certificate_image')['sizes']['large']; ?>" /></a>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</div>