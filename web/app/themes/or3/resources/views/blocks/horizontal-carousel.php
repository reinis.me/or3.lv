<section class="horizontal-carousel">
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10">
            <div class="horizontal-images-carousel  <?php if(get_field('hide_text_and_controls')) {echo "content-hidden";} ?>" 
            id="sldr_c_<?php echo $block['id']; ?>" 
            data-navfor="#sldr_m_<?php echo $block['id']; ?>"
            <?php if(get_field('autoplay')) { ?> data-autoplay="2000" <?php } ?> 

            >
                <?php if (have_rows('slides')) : ?>
                    <?php while (have_rows('slides')) : the_row();
                    ?>

                        <div class="slide">
                            <div class="horizontal-carousel-img lazy" data-bg="<?php echo wp_get_attachment_image_src(get_sub_field('image'), '™')[0]; ?>"></div>
                            
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="horizontal-content-carousel <?php if(get_field('hide_text_and_controls')) {echo "hidden";} ?>" 
            id="sldr_m_<?php echo $block['id']; ?>" 
            data-navfor="#sldr_c_<?php echo $block['id']; ?>"
            >

                <?php if (have_rows('slides')) : ?>
                    <?php while (have_rows('slides')) : the_row();
                    ?>

                        <div class="slide">
                            <div class="inner">
                            <?php if (get_sub_field('link')) { ?>
                                <a href="<?php the_sub_field('link'); ?>">
                             <?php } ?>
                                <div class="mini-title"><?php the_sub_field('mini_title'); ?></div>
                                <div class="title"><?php the_sub_field('title'); ?></div>
                                <div class="content"><?php the_sub_field('content'); ?></div>
                                <?php if (get_sub_field('link')) { ?>
                                </a>
                             <?php } ?>
                            </div>
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>

            </div>

        </div>
    </div>
</section>