<section class="key-facts">

        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">

                <div class="row">

                <div class="col-12 col-md-6 order-md-2">
                        <div class="key-facts-image-carousel" id="sldr_c_<?php echo $block['id']; ?>" data-navfor="#sldr_m_<?php echo $block['id']; ?>">
                            <?php if (have_rows('slides')) : ?>
                                <?php while (have_rows('slides')) : the_row();
                                ?>

                                    <div class="slide">
                                        <div class="key-facts-img lazy" data-bg="<?php echo wp_get_attachment_image_src(get_sub_field('image'), 'large')[0]; ?>"></div>
                    
                                    </div>

                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 order-md-1 key-facts-col">
                        <div class="key-facts-title">
                            <?php the_field('title'); ?>
                        </div>

                        <div class="key-facts-carousel-holder">
                            <div class="key-fatcs-carousel" id="sldr_m_<?php echo $block['id']; ?>" data-navfor="#sldr_c_<?php echo $block['id']; ?>">
                                <?php if (have_rows('slides')) : ?>
                                    <?php while (have_rows('slides')) : the_row();
                                    ?>

                                        <div class="slide">
                                            <div class="inner">
                                                <div class="key-fact-title"><?php the_sub_field('title'); ?></div>
                                                <div class="key-fact-content"><?php the_sub_field('content'); ?></div>
                                            </div>
                                        </div>

                                    <?php endwhile; ?>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
    </div>
</section>