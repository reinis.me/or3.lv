<div class="value-tree">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-10">
                <h2><?php the_field('title'); ?></h2>

                <div class="items">
                    <?php if (have_rows('items')) : ?>
                        <?php while (have_rows('items')) : the_row();
                        ?>
                            <div class="value-tree-item">
                                <div class="value-tree-col col-title">
                                    <div class="inner">
                                        <?php the_sub_field('title'); ?>
                                    </div>
                                </div>
                                <div class="value-tree-col col-content">
                                    <div class="inner">
                                        <?php the_sub_field('content'); ?>
                                    </div>
                                </div>

                                <div class="dot"></div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

</div>