<article @php post_class() @endphp>
  <header>
    <div class="container reading-container">
    <h1 class="entry-title">{!! get_the_title() !!}</h1>
    </div>
    {{-- @include('partials/entry-meta') --}}
  </header>
  <div class="entry-content">
    <div class="container reading-container">
    @php the_content() @endphp

    <div class="soc-share">
    
      <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="share-btn share-fb">Share this article on Facebok</a>
      <a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=" target="_blank" class="share-btn share-tw">Share this article on Twitter</a>
      <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>" target="_blank" class="share-btn share-li">Share this article on LinkedIn</a>
    
    </div>

    </div>
  </div>
  <footer>
    {{-- {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!} --}}
  </footer>
  {{-- @php comments_template('/partials/comments.blade.php') @endphp --}}
</article>
