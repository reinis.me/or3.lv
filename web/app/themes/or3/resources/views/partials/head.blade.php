<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
  @php wp_head() @endphp

  <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

  <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/32cc611236403b4b9bbc9ac0b/c8d4e224ba21325940fb1afa9.js");</script>

</head>
