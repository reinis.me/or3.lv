<div class="circle-image-container">
    <div class="circle-image-decoration">
    </div>
    <div class="circle-image-image">
        <?php if (is_array($images) && count($images) == 1) { 
            echo wp_get_attachment_image( $images[0], 'image-circle');
         } elseif (is_array($images) && count($images) > 1) { ?>
        <div class="circle-image-carousel">
            <?php foreach ($images as $key => $value) { ?>
                <div class="circle-slide">
                    <?php
            echo wp_get_attachment_image( $value, 'image-circle'); ?>
                    </div>
            <?php
            } ?>
    
        </div>
        <?php } ?>
    </div>
</div>
<div class="circle-image-arrows"></div> 