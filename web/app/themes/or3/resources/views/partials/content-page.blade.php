<?php if (get_field( "show_page_hero", $post->ID )){ 

 if( have_rows('page_hero_content') ): 

 add_filter( 'body_class', function( $classes ) {
    return array_merge( $classes, array( 'page-has-hero' ) );
} );

 while( have_rows('page_hero_content') ): the_row(); ?>

<div class="page-hero"
    style="background-color: <?php the_sub_field('page_hero_background_color'); ?>; background-image: url(<?php echo wp_get_attachment_image_src(get_sub_field('page_hero_background_image'), 'full')[0]; ?>)">
    <div class="page-hero-container">
        <?php the_sub_field('upper_title'); ?>
        <div class="main-title-items">
        <?php 
         if( have_rows('main_title') ): 
         while( have_rows('main_title') ): the_row(); ?>

<h1><?php echo nl2br(get_sub_field('main_title_item')); ?></h1>

        <?php 
        endwhile; 
        endif;
        ?>
        </div>


        <?php the_sub_field('page_hero_text'); ?>

    </div>
    <div class="scroll-down"><span>Scroll down</span></div>
</div>

<?php endwhile; ?>
<?php endif; 

    }
?>
<div class="container container-outer mt-4 mb-4">
    @php the_content() @endphp
</div>

{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav">
    <p>' . __('Pages:', 'sage'), 'after' => '</p>
</nav>']) !!}