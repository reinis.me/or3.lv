<header class="header-wrap">
  <div class="container header-container">

    <div class="d-flex header-top-bar">
      <div class="col-logo">
        <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
      </div>
      <div class="flex-grow-1 col-main-nav">
        <div class="main-nav-container">
          <?php             
          wp_nav_menu(array('menu' => get_field('header_main_menu', 'option')));
          ?>
        </div>
      </div>
      <div class="col-menu-btn">
        <button type="button" class="toggle-menu-btn open-menu">
          <span></span>
          <span></span>
          <span></span>
        </button>
      </div>
    </div>

    <?php if (is_post_type_archive('products')) : ?>
    <div class="sliding-menu">
      <div class="inner">
      <?php
    $product_categories = get_terms( array(
        'taxonomy' => 'product-categories',
        'hide_empty' => false,
    ) );
    foreach ($product_categories as $cat) {
    ?>
        <a class="sliding-menu-link"
            href="#<?php echo $cat->slug; ?>" title="<?php echo $cat->name; ?>" data-sliding-id="<?php echo $cat->slug; ?>">
            <?php echo $cat->name; ?>
        </a>

        <?php
    }
      ?>

    </div>
  </div>


    <?php endif; ?>
    <?php if (is_post_type_archive('services')) : ?>
    
    <div class="sliding-menu">
      <div class="inner">

        <?php 
        global $query_string;
        $cat_posts = new WP_Query($query_string."&orderby=menu_order&order=ASC");
        ?>


        @while ($cat_posts->have_posts()) @php
        $cat_posts->the_post();
        global $post;
        $slug = $post->post_name;
        @endphp
        <a class="sliding-menu-link"
        href="#<?php echo $slug; ?>" title="<?php the_title(); ?>" data-sliding-id="<?php echo $slug; ?>">
        <?php the_title(); ?>
    </a>
        @endwhile


    </div>
  </div>


    <?php endif; ?>
    <?php if (is_tax('product-categories')) : ?>
    
    <div class="sliding-menu">
      <div class="inner">

        <?php 
        global $query_string;
        $cat_posts = new WP_Query($query_string."&orderby=menu_order&order=ASC");
        ?>


        @while ($cat_posts->have_posts()) @php
        $cat_posts->the_post();
        global $post;
        $slug = $post->post_name;
        @endphp
        <a class="sliding-menu-link"
        href="#<?php echo $slug; ?>" title="<?php the_title(); ?>" data-sliding-id="<?php echo $slug; ?>">
        <?php the_title(); ?>
    </a>
        @endwhile


    </div>
  </div>


    <?php endif; ?>
    <?php ?>
    <?php ?>



  </div>

  <div class="header-extended">
    <div class="container header-container">
      <div class="row">
        <div class="ext-col col-12 col-md">
          <div class="inner"> 
            <?php             
            wp_nav_menu(array('menu' => get_field('header_main_menu', 'option')));
            ?>
          </div>
        </div>
        <div class="ext-col col-12 col-md">
          <div class="inner">

            <div class="menu-column-title"><?php the_field('header_secondary_menu_title', 'option'); ?></div>
            <?php wp_nav_menu(array('menu' => get_field('header_secondary_menu', 'option')));?>
          </div>
        </div>
        <div class="ext-col col-12 col-md">
          <div class="inner">
            <div class="menu-column-title"><?php the_field('header_contacts_title', 'option'); ?></div>
            <?php the_field('header_contacts_content', 'option'); ?>
          </div>
        </div>
      </div>
      <button type="button" class="close-menu"></button>
    </div>
  </div>
</header>