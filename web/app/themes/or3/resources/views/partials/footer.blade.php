<footer class="page-footer">
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-10">
                <div class="row">


                    <?php if (have_rows('footer_columns', 'option')): ?>
                    <?php while (have_rows('footer_columns', 'option')):

                    the_row();

                    // Get sub field values.
                    // $image = get_sub_field('image');
                    $footer_column_content_type = get_sub_field('footer_column_content_type');
                    ?>

                    <div
                        class="col-12 col-sm-6 col-lg-3 footer-col footer-column-<?php echo $footer_column_content_type; ?>">
                        <h5><?php the_sub_field('footer_column_title'); ?></h5>

                        <div class="footer-col-content">
                            <?php if ($footer_column_content_type == 'wysiwyg') { ?>
                            <?php the_sub_field('footer_column_content'); ?>
                            <?php } elseif ($footer_column_content_type == 'menu') { ?>

                            <?php wp_nav_menu(['menu' => get_sub_field('footer_column_menu')]); ?>

                            <?php } elseif ($footer_column_content_type == 'subscribe') { ?>


                            <!-- Begin Mailchimp Signup Form -->
                            <div id="mc_embed_signup">
                                <form
                                    action="https://or3.us7.list-manage.com/subscribe/post?u=32cc611236403b4b9bbc9ac0b&amp;id=8a51c36e60"
                                    method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
                                    class="validate" target="_blank" novalidate>
                                    <div id="mc_embed_signup_scroll">

                                        <div class="mc-field-group form-group">
                                            <input type="email" value="" name="EMAIL"
                                                class="required email form-control" id="mce-EMAIL">
                                            <label for="mce-EMAIL">Email Address </label>
                                        </div>
                                        <div id="mce-responses" class="clear">
                                            <div class="response" id="mce-error-response" style="display:none"></div>
                                            <div class="response" id="mce-success-response" style="display:none"></div>
                                        </div>
                                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input
                                                type="text" name="b_32cc611236403b4b9bbc9ac0b_8a51c36e60" tabindex="-1"
                                                value=""></div>
                                        <div class="clear"><input type="submit" value="Subscribe" name="subscribe"
                                                id="mc-embedded-subscribe" class="btn btn-primary"></div>
                                    </div>
                                </form>
                            </div>

                            <!--End mc_embed_signup-->

                            <?php } ?>

                        </div>

                    </div>


                    <?php
                    endwhile; ?>
                    <?php endif; ?>

                </div>
                <div class="footer-sub">
                    <div class="row">
                        <div class="col">
                            <a href="/" class="footer-logo"></a>
                        </div>
                        <div class="col">

                            <?php if (have_rows('social_links', 'option')): ?>
                            <div class="footer-social-links">
                                <?php while (have_rows('social_links', 'option')):
                                the_row(); ?>

                                <a href="<?php the_sub_field('social_url'); ?>"
                                    class="social-link"
                                    style="background-image: url(<?php the_sub_field('social_icon'); ?>)"
                                    title="<?php the_sub_field('social_title'); ?>"
                                    target="_blank"><?php the_sub_field('social_title'); ?></a>

                                <?php
                                endwhile; ?>
                            </div>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
            {{-- @php dynamic_sidebar('sidebar-footer') @endphp --}}
        </div>
</footer>

@if (!isset($_COOKIE['or3_cookies']))
<div class="cookie-consent">
    <div class="container">
        <div class="consent-inner">
            <div class="txt">We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. </div>
            <button class="btn btn-primary cookies-ok">OK</button>
        </div>
    </div>
</div>
    
@endif
