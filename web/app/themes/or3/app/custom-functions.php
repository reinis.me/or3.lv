<?php


function or3_custom_post_type() {

    $category_labels = array(
		'name'                       => _x( 'Product categories', 'taxonomy general name', 'or3' ),
		'singular_name'              => _x( 'Category', 'taxonomy singular name', 'or3' ),
		'search_items'               => __( 'Search categories', 'or3' ),
		'popular_items'              => __( 'Popular categories', 'or3' ),
		'all_items'                  => __( 'All categories', 'or3' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit category', 'or3' ),
		'update_item'                => __( 'Update category', 'or3' ),
		'add_new_item'               => __( 'Add New category', 'or3' ),
		'new_item_name'              => __( 'New category Name', 'or3' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'or3' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'or3' ),
		'choose_from_most_used'      => __( 'Choose from the most used categories', 'or3' ),
		'not_found'                  => __( 'No categories found.', 'or3' ),
		'menu_name'                  => __( 'Categories', 'or3' ),
    );


    
    register_taxonomy(
		'product-categories',
		'products',
		array(
            'labels' => $category_labels,
			'rewrite' => array( 'slug' => 'products' ),
			'hierarchical' => true,
			'show_in_rest' => true,
		)
    );
   
    $supports = array(
    'title', // post title
    'editor', // post content
    'author', // post author
    'thumbnail', // featured images
    'custom-fields', // custom fields
    'revisions', // post revisions
    );

    $labels = array(
    'name' => _x('Products', 'plural', 'or3' ),
    'singular_name' => _x('Product', 'singular', 'or3' ),
    'menu_name' => _x('Products', 'admin menu', 'or3' ),
    'name_admin_bar' => _x('Products', 'admin bar', 'or3' ),
    'add_new' => _x('Add New', 'add new', 'or3' ),
    'add_new_item' => __('Add New Product', 'or3' ),
    'new_item' => __('New Product', 'or3' ),
    'edit_item' => __('Edit Product', 'or3' ),
    'view_item' => __('View Product', 'or3' ),
    'all_items' => __('All Products', 'or3' ),
    'search_items' => __('Search Products', 'or3' ),
    'not_found' => __('No Products found.', 'or3' ),
    );

    $args = array(
	'supports' => $supports,
	'show_in_rest' => true,
    'labels' => $labels,
    'public' => true,
    'query_var' => true,
    'menu_icon' => 'dashicons-cart',
    'rewrite' => array('slug' => 'products'),
    'has_archive' => true,
    'hierarchical' => true,
    );

    register_post_type('products', $args);


    $labels = array(
        'name' => _x('Services', 'plural', 'or3' ),
        'singular_name' => _x('Service', 'singular', 'or3' ),
        'menu_name' => _x('Services', 'admin menu', 'or3' ),
        'name_admin_bar' => _x('Services', 'admin bar', 'or3' ),
        'add_new' => _x('Add New', 'add new', 'or3' ),
        'add_new_item' => __('Add New Service', 'or3' ),
        'new_item' => __('New Service', 'or3' ),
        'edit_item' => __('Edit Service', 'or3' ),
        'view_item' => __('View Service', 'or3' ),
        'all_items' => __('All Services', 'or3' ),
        'search_items' => __('Search Services', 'or3' ),
        'not_found' => __('No Services found.', 'or3' ),
        );
    
        $args = array(
        'supports' => $supports,
        'show_in_rest' => true,
        'labels' => $labels,
        'public' => true,
        'query_var' => true,
        'menu_icon' => 'dashicons-hammer',
        'rewrite' => array('slug' => 'services'),
        'has_archive' => true,
        'hierarchical' => true,
        );
    
        register_post_type('services', $args);

}

add_action('init', 'or3_custom_post_type');


add_action( 'admin_head-edit-tags.php', 'wpse_58799_remove_parent_category' );

function wpse_58799_remove_parent_category()
{
    if ( 'product-categories' != $_GET['taxonomy'] )
        return;

    $parent = 'parent()';

    if ( isset( $_GET['action'] ) )
        $parent = 'parent().parent()';

    ?>
        <script type="text/javascript">
            jQuery(document).ready(function($)
            {     
                $('label[for=parent]').<?php echo $parent; ?>.remove();       
                $('.term-description-wrap').remove();   
                $('#col-left, #col-right').css('width', '100%');    
            });
        </script>
    <?php
}

add_image_size( 'image-circle', 650, 650, true );
add_image_size( 'key-facts', 704, 850, true );
add_image_size( 'horizontal-carousel', 1425, 465, true );


/**
 * Pass in a taxonomy value that is supported by WP's `get_taxonomy`
 * and you will get back the url to the archive view.
 * @param $taxonomy string|int
 * @return string
 */
function get_taxonomy_archive_link( $taxonomy ) {
    $tax = get_taxonomy( $taxonomy ) ;
    return get_bloginfo( 'url' ) . '/' . $tax->rewrite['slug'];
  }
  

  add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {


        acf_register_block_type(array(
            'name'              => 'or3_circular_carousel',
            'title'             => __('OR3 circle animation', 'or3'),
            'description'       => __('Displays an animated circle carousel', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/circular-carousel.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));


        acf_register_block_type(array(
            'name'              => 'or3_about_boxes',
            'title'             => __('OR3 About boxes ', 'or3'),
            'description'       => __('Box with an icon aside', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/about-boxes.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));


        acf_register_block_type(array(
            'name'              => 'or3_horizontal_carousel',
            'title'             => __('OR3 Horizontal carousel ', 'or3'),
            'description'       => __('', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/horizontal-carousel.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));


        acf_register_block_type(array(
            'name'              => 'or3_management_carousel',
            'title'             => __('OR3 Management carousel', 'or3'),
            'description'       => __('Displays a carousel of management members', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/management-carousel.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));


        acf_register_block_type(array(
            'name'              => 'or3_our_focus',
            'title'             => __('OR3 Our focus', 'or3'),
            'description'       => __('Block for "Our focus" element', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/our-focus.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));


        acf_register_block_type(array(
            'name'              => 'or3_sustainability',
            'title'             => __('OR3 Sustainaibiliy', 'or3'),
            'description'       => __('Block for "Sustainaibiliy" element', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/sustainability.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));


        acf_register_block_type(array(
            'name'              => 'or3_testimonials',
            'title'             => __('OR3 Testimonials', 'or3'),
            'description'       => __('Testimonials carousel', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/testimonials.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));


        acf_register_block_type(array(
            'name'              => 'or3_value_tree',
            'title'             => __('OR3 Value tree', 'or3'),
            'description'       => __('Displays a tree of company values', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/value-tree.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));


        acf_register_block_type(array(
            'name'              => 'or3_vertical_carousel',
            'title'             => __('OR3 Key Facts', 'or3'),
            'description'       => __('', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/vertical-carousel.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));


        acf_register_block_type(array(
            'name'              => 'or3_why',
            'title'             => __('OR3 Why people trust', 'or3'),
            'description'       => __('', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/why.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));

        acf_register_block_type(array(
            'name'              => 'or3_video',
            'title'             => __('OR3 Video', 'or3'),
            'description'       => __('', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/video.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));

        acf_register_block_type(array(
            'name'              => 'or3_quality',
            'title'             => __('OR3 Quality and safety', 'or3'),
            'description'       => __('', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/quality.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));

        acf_register_block_type(array(
            'name'              => 'or3_certs',
            'title'             => __('OR3 Certificates', 'or3'),
            'description'       => __('', 'or3'),
            'render_template'   => get_template_directory() . '/views/blocks/certs.php',
            'category'          => 'widgets',
            'icon'              => 'clipboard',
            'keywords'          => array( 'or3' ),
        ));

    }
}


function alter_menu($items, $args)
{

    if(is_tax('product-categories')) {

  // loop
  foreach ($items as &$item) {


      if (in_array("products-link", $item->classes)) {
        $item->classes[] = 'current-menu-ancestor';
     }
    }

  }


  // return
  return $items;
}

add_filter('wp_nav_menu_objects', 'alter_menu', 10, 2);


function pre($stuff)
{
  echo "<pre>";
  print_r($stuff);
  echo "</pre>";
}


// function wpb_filter_query( $query, $error = true ) {
//     if ( is_search() ) {
//     $query->is_search = false;
//     $query->query_vars[s] = false;
//     $query->query[s] = false;
//     if ( $error == true )
//     $query->is_404 = true;
//     }
//     }

//     add_action( 'parse_query', 'wpb_filter_query' );

    // add_action( 'widgets_init', 'remove_search_widget' );



    
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
