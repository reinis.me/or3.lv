<?php



$all_menus = [];

foreach (wp_get_nav_menus() as $menu) {

    $all_menus[$menu->term_id] = $menu->name;

}



if( function_exists('acf_add_options_page') ) {
	

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Header and main menu Settings',
		'menu_title'	=> 'Header & Menu',
		'parent'	=> 'themes.php',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer Settings and Content',
		'menu_title'	=> 'Footer',
		'parent'	=> 'themes.php',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Services landing page content',
		'menu_title'	=> 'Landing page',
		'parent'	=> 'edit.php?post_type=services',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Products landing page content',
		'menu_title'	=> 'Landing page',
		'parent'	=> 'edit.php?post_type=products',
	));
	
}


if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5fddc07492990',
        'title' => 'Footer content',
        'fields' => array(
            array(
                'key' => 'field_5fddc085c5271',
                'label' => 'Footer columns',
                'name' => 'footer_columns',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'block',
                'button_label' => '',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5fddc09ec5272',
                        'label' => 'Column title',
                        'name' => 'footer_column_title',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5fddc0afc5273',
                        'label' => 'Footer column content type',
                        'name' => 'footer_column_content_type',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'wysiwyg' => 'Free content',
                            'menu' => 'Menu',
                            'subscribe' => 'Subscription form',
                        ),
                        'default_value' => false,
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'field_5fddc125c5274',
                        'label' => 'Content',
                        'name' => 'footer_column_content',
                        'type' => 'wysiwyg',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5fddc0afc5273',
                                    'operator' => '==',
                                    'value' => 'wysiwyg',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'tabs' => 'all',
                        'toolbar' => 'full',
                        'media_upload' => 0,
                        'delay' => 0,
                    ),
                    array(
                        'key' => 'field_5fddc154c5275',
                        'label' => 'Menu',
                        'name' => 'footer_column_menu',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => array(
                            array(
                                array(
                                    'field' => 'field_5fddc0afc5273',
                                    'operator' => '==',
                                    'value' => 'menu',
                                ),
                            ),
                        ),
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => $all_menus,
                        'default_value' => false,
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                ),
            ),
            array(
                'key' => 'field_5fddc5b07d797',
                'label' => 'Social links',
                'name' => 'social_links',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'table',
                'button_label' => '',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5fddc5d47d798',
                        'label' => 'Icon',
                        'name' => 'social_icon',
                        'type' => 'image',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'return_format' => 'url',
                        'preview_size' => 'thumbnail',
                        'library' => 'all',
                        'min_width' => '',
                        'min_height' => '',
                        'min_size' => '',
                        'max_width' => '',
                        'max_height' => '',
                        'max_size' => '',
                        'mime_types' => '',
                    ),
                    array(
                        'key' => 'field_5fddc5f07d799',
                        'label' => 'Title',
                        'name' => 'social_title',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5fddc6017d79a',
                        'label' => 'URL',
                        'name' => 'social_url',
                        'type' => 'url',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                    ),
                ),
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-footer',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));





// acf_add_local_field_group(array(
// 	'key' => 'group_5fde0b7812328',
// 	'title' => 'Extra content',
// 	'fields' => array(
// 		array(
// 			'key' => 'field_5fde0b8aab750',
// 			'label' => 'Show page hero',
// 			'name' => 'show_page_hero',
// 			'type' => 'true_false',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'message' => '',
// 			'default_value' => 0,
// 			'ui' => 1,
// 			'ui_on_text' => 'Yes',
// 			'ui_off_text' => 'No',
// 		),
// 		array(
// 			'key' => 'field_5fe0b14681303',
// 			'label' => 'Page hero content',
// 			'name' => 'page_hero_content',
// 			'type' => 'group',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => array(
// 				array(
// 					array(
// 						'field' => 'field_5fde0b8aab750',
// 						'operator' => '==',
// 						'value' => '1',
// 					),
// 				),
// 			),
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'layout' => 'block',
// 			'sub_fields' => array(
// 				array(
// 					'key' => 'field_5fe0b15781304',
// 					'label' => 'Page hero text',
// 					'name' => 'page_hero_text',
// 					'type' => 'wysiwyg',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array(
// 						'width' => '',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'default_value' => '',
// 					'tabs' => 'all',
// 					'toolbar' => 'full',
// 					'media_upload' => 1,
// 					'delay' => 0,
// 				),
// 				array(
// 					'key' => 'field_5fe0b18481305',
// 					'label' => 'Page hero background image',
// 					'name' => 'page_hero_background_image',
// 					'type' => 'image',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array(
// 						'width' => '',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'return_format' => 'id',
// 					'preview_size' => 'medium',
// 					'library' => 'all',
// 					'min_width' => '',
// 					'min_height' => '',
// 					'min_size' => '',
// 					'max_width' => '',
// 					'max_height' => '',
// 					'max_size' => '',
// 					'mime_types' => '',
// 				),
// 				array(
// 					'key' => 'field_5fe0b28887b24',
// 					'label' => 'Page hero background color',
// 					'name' => 'page_hero_background_color',
// 					'type' => 'color_picker',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array(
// 						'width' => '',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'default_value' => '',
// 				),
// 			),
// 		),
// 		array(
// 			'key' => 'field_5fe0b7a36fffc',
// 			'label' => 'Show sticky button',
// 			'name' => 'show_sticky_button',
// 			'type' => 'true_false',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => 0,
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'message' => '',
// 			'default_value' => 0,
// 			'ui' => 1,
// 			'ui_on_text' => 'Yes',
// 			'ui_off_text' => 'No',
// 		),
// 		array(
// 			'key' => 'field_5fe0b7d66fffd',
// 			'label' => 'Sicky button content',
// 			'name' => 'sicky_button_content',
// 			'type' => 'group',
// 			'instructions' => '',
// 			'required' => 0,
// 			'conditional_logic' => array(
// 				array(
// 					array(
// 						'field' => 'field_5fe0b7a36fffc',
// 						'operator' => '==',
// 						'value' => '1',
// 					),
// 				),
// 			),
// 			'wrapper' => array(
// 				'width' => '',
// 				'class' => '',
// 				'id' => '',
// 			),
// 			'layout' => 'block',
// 			'sub_fields' => array(
// 				array(
// 					'key' => 'field_5fe0b7f86ffff',
// 					'label' => 'Sticky button link',
// 					'name' => 'sticky_button_url',
// 					'type' => 'link',
// 					'instructions' => '',
// 					'required' => 0,
// 					'conditional_logic' => 0,
// 					'wrapper' => array(
// 						'width' => '',
// 						'class' => '',
// 						'id' => '',
// 					),
// 					'return_format' => 'url',
// 				),
// 			),
// 		),
// 	),
// 	'location' => array(
// 		array(
// 			array(
// 				'param' => 'post_type',
// 				'operator' => '==',
// 				'value' => 'page',
// 			),
// 		),
// 	),
// 	'menu_order' => 0,
// 	'position' => 'acf_after_title',
// 	'style' => 'default',
// 	'label_placement' => 'top',
// 	'instruction_placement' => 'label',
// 	'hide_on_screen' => '',
// 	'active' => true,
// 	'description' => '',
// ));

acf_add_local_field_group(array(
	'key' => 'group_5fe0b2a09bc68',
	'title' => 'Header and main menu Settings',
	'fields' => array(
		array(
			'key' => 'field_5fe0b65392c82',
			'label' => 'Main menu',
			'name' => 'header_main_menu',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
            'choices' => $all_menus,
			'default_value' => false,
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_5fe0b66c92c83',
			'label' => 'Secondary menu title',
			'name' => 'header_secondary_menu_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5fe0b67992c84',
			'label' => 'Secondary menu',
			'name' => 'header_secondary_menu',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
            'choices' => $all_menus,
			'default_value' => false,
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_5fe0b69992c85',
			'label' => 'Contacts title',
			'name' => 'header_contacts_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5fe0b6a792c86',
			'label' => 'Contacts content',
			'name' => 'header_contacts_content',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'all',
			'toolbar' => 'full',
			'media_upload' => 0,
			'delay' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-header-menu',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));
    
    endif;